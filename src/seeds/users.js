import service from './service';

const seed = async() => {
  const users = [{
      username: 'mynameiswho',
      password: 'slimshady',
    },
  ];
  const usersService = await service('users');
  await usersService.remove(null);
  return usersService.create(users);
};

export default seed;