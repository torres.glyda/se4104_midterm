import setupServer from '../backend/setupServer';

const seed = async() => {
  const app = await setupServer();
  
  const choices = await app.service('/api/choices').find();
  const choicesIds = choices.map(choice => choice._id);

  const questions = [{
        questions: 'Is this a question?',
        choicesIds: choicesIds.slice(0, 4),
        correctAnswerID: choicesIds.slice(0, 4)[1],
      }, {
        questions: 'Who\'s our National Hero?',
        choicesIds: choicesIds.slice(4, 8),
        correctAnswerID: choicesIds.slice(4, 8)[0],
      }, {
        questions: '1 + 2 = ?',
        choicesIds: choicesIds.slice(8, 12),
        correctAnswerID: choicesIds.slice(8, 12)[3],
      }, {
        questions: 'Which does not belong to the group?',
        choicesIds: choicesIds.slice(12, 16),
        correctAnswerID: choicesIds.slice(12, 16)[0],
      }, {
        questions: 'Which is not a fruit?',
        choicesIds: choicesIds.slice(16, 20),
        correctAnswerID: choicesIds.slice(16, 20)[2],
      }, {
        questions: 'Which of the following is liquid?',
        choicesIds: choicesIds.slice(20, 24),
        correctAnswerID: choicesIds.slice(20, 24)[1],
      }, {
        questions: 'What\'s up?',
        choicesIds: choicesIds.slice(24, 28),
        correctAnswerID: choicesIds.slice(24, 28)[3],
      }, {
        questions: 'Which is the correct answer?',
        choicesIds: choicesIds.slice(28, 32),
        correctAnswerID: choicesIds.slice(28, 32)[2],
      }, {
        questions: 'Which came first?',
        choicesIds: choicesIds.slice(32, 36),
        correctAnswerID: choicesIds.slice(32, 36)[3],
      }, {
        questions: 'What planet is closest to the moon?',
        choicesIds: choicesIds.slice(36, 40),
        correctAnswerID: choicesIds.slice(36, 40)[0],
    },
];

const questionsService = await app.service('/api/questions');
await questionsService.remove(null);
return questionsService.create(questions);
};

export default seed;