import setupServer from '../backend/setupServer';

const seed = async() => {
  const app = await setupServer();
  
  const questions = await app.service('/api/questions').find();
  const questionsIds = questions.map(choice => choice._id);

  const tests = [{
      questionsIds: questionsIds.slice(0, 5)
    },
    {
      questionsIds: questionsIds.slice(5, 10)
    },
  ];

  const testsService = await app.service('/api/tests');
  await testsService.remove(null);
  return testsService.create(tests);
};

export default seed;