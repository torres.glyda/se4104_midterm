import path from 'path';
import fs from 'fs';

const runSeeds = () => {
  const collectionName = process.argv[2];
  const location = path.join(process.cwd(), `src/seeds/${collectionName}.js`);
  const fileExists = fs.existsSync(location);

  if (fileExists) {
    const seed = require(`./${collectionName}`).default;
    (async() => {
      try {
        await seed();
        console.log(`${collectionName} seed done.`);
      } catch (error) {
        console.log('ERR: ', error);
      }
      process.exit(0);
    })();
  } else {
    console.log(`${collectionName} file is invalid.`);
  }
};

runSeeds();