import setupServer from '../backend/setupServer';

const seed = async() => {
  const app = await setupServer();

  const choices = [{
    choice: 'Yes'
  }, {
    choice: 'No'
  }, {
    choice: 'Maybe'
  }, {
    choice: 'Idk'
  }, {
    choice: 'Rizal'
  }, {
    choice: 'Mabini'
  }, {
    choice: 'Magellan'
  }, {
    choice: 'Aguinaldo'
  }, {
    choice: '0'
  }, {
    choice: '1'
  }, {
    choice: '2'
  }, {
    choice: '3'
  }, {
    choice: 'fish'
  }, {
    choice: 'dog'
  }, {
    choice: 'cat'
  }, {
    choice: 'lion'
  }, {
    choice: 'apple'
  }, {
    choice: 'banana'
  }, {
    choice: 'carrot'
  }, {
    choice: 'kiwi'
  }, {
    choice: 'ice'
  }, {
    choice: 'water'
  }, {
    choice: 'air'
  }, {
    choice: 'pencil'
  }, {
    choice: 'Nothing much.'
  }, {
    choice: 'The usual.'
  }, {
    choice: 'Air.'
  }, {
    choice: 'The ceiling.'
  }, {
    choice: 'Not this'
  }, {
    choice: 'This is not it'
  }, {
    choice: 'This one'
  }, {
    choice: 'Go back!'
  }, {
    choice: 'chicken'
  }, {
    choice: 'sun'
  }, {
    choice: 'egg'
  }, {
    choice: 'moon'
  }, {
    choice: 'earth'
  }, {
    choice: 'mercury'
  }, {
    choice: 'mars'
  }, {
    choice: 'neptune'
  }];

  const choicesService = await app.service('api/choices');
  await choicesService.remove(null);
  return choicesService.create(choices);
};

export default seed;