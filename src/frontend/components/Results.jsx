import React from 'react';
import { observer, inject } from 'mobx-react';
import { Link } from 'react-router-dom';

import { Container, Button } from '../styles/components';

const ResultsComponent = inject('testStore')(observer(({ testStore, testNumber }) => (
	<Container>
		<p>Your score is</p>
		<h1>{testStore.scoreDetails}</h1>
		<Link to={`/test`} className='link'>
			<Button onClick={testStore.resetTest} primary>Try Again?</Button>
		</Link>
	</Container>
)));

export default ResultsComponent;