import React from 'react';
import { observer, inject } from 'mobx-react';
import { Link } from 'react-router-dom';

import LoginComponent from './Login';
import TestListComponent from './TestList';
import { Button } from '../styles/components';

const HomeComponent = inject('userStore', 'authStore')(observer(({ userStore, authStore }) => (
	<div>
		{userStore.currentUser
			? <div>
				Sup, {userStore.currentUser.username}?
				<Link to={`/`} className='link'>
					<Button onClick={authStore.handleLogout}>LOGOUT</Button >
				</Link>
				<TestListComponent />
			</div>
			: <LoginComponent />}
	</div>
)));

export default HomeComponent;