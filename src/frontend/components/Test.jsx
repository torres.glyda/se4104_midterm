import React from 'react';
import { observer, inject } from 'mobx-react';
import { Link } from 'react-router-dom';

import { Button } from '../styles/components';

import QuestionsComponent from './Questions';

const TestComponent = inject('testStore')(observer(({ test, testNumber, testStore }) => (
	<div>
		<h1>Test #{testNumber}</h1>
		{test.questions.map((question, index) =>
			<QuestionsComponent key={question._id} question={question} questionIndex={index} />
		)}
		<Link to={`/test/${testNumber}/results`} className='link' key={test._id}>
			<Button>Check Results</Button>
		</Link>
	</div>
)));

export default TestComponent;