import React from 'react';
import { observer, inject } from 'mobx-react';

import { Input, Button } from '../styles/components';

const LoginComponent = inject('authStore')(observer(({ authStore }) => (
	<div>
		<span> Username </span> <Input type='text' id='username' onChange={authStore.setValue} />
		<span> Password </span> <Input type='password' id='password' onChange={authStore.setValue} />
		<Button onClick={authStore.handleLogin}>LOGIN</Button>
	</div>
)));

export default LoginComponent;