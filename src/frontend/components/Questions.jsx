import React from 'react';
import { observer } from 'mobx-react';

import { Question, Container } from '../styles/components';

import ChoicesComponent from './Choices';

const QuestionComponent = observer(({ question, questionIndex }) => (
	<Container>
		<p>	{question.questions}	</p>
		<ChoicesComponent choices={question.choices} questionId={question._id} questionIndex={questionIndex} />
	</Container>
));

export default QuestionComponent;