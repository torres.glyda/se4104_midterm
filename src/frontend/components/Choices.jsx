import React from 'react';
import { observer, inject } from 'mobx-react';

import { Choice } from '../styles/components';

const ChoicesComponent = inject('testStore')(observer(({ choices, questionId, questionIndex, testStore }) => (
	<div >
		{choices.map(choice =>
			<Choice key={choice._id}>
				<input type='radio'
					id={choice._id}
					name={`choices-${questionIndex}`}
					onChange={() => testStore.selectChoice(questionId, choice)}
					checked={testStore.selectedTestUserChoices.userChoicesIds.includes(choice._id)}
				/>
				<label htmlFor={choice._id}>	<span> {choice.choice} </span> </label>
			</Choice>
		)}
	</div>
)));

export default ChoicesComponent;