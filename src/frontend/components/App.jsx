import React from 'react';
import { observer, inject } from 'mobx-react';
import { HashRouter as Router, Route } from 'react-router-dom';

import { AppContainer } from '../styles/components';
import TestsListComponent from './TestList';
import TestComponent from './Test';
import ResultsDisplay from './Results';
import HomeComponent from './Home';

const App = inject('testStore', 'userStore')(observer(({ testStore, userStore }) => (
	<Router>
		<AppContainer>
			<Route path='/' component={HomeComponent} />
			{userStore.currentUser
				? testStore.tests.map((test, index) =>
					<div key={test._id} >
						<Route path={`/test/${index + 1}`} component={() =>
							<TestComponent test={test} testNumber={index + 1} onLoad={testStore.selectTest(test)} />
						} />
						<Route path={`/test/${index + 1}/results`} component={() =>
							<ResultsDisplay testNumber={index + 1} />
						} />
					</div>)
				: ''}
		</AppContainer>
	</Router>
)));

export default App;