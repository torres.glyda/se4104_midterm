import React from 'react';
import { observer, inject } from 'mobx-react';
import { Link } from 'react-router-dom';

import { TestListItem } from '../styles/components';

const TestListComponent = inject('testStore')(observer(({ testStore }) => (
	<div>
		<h1>Select a test:</h1>
		{testStore.tests.map((test, index) =>
			<Link to={`/test/${index + 1}`} className='link' key={test._id}>
				<TestListItem>
					Test #{index + 1}
				</TestListItem>
			</Link>
		)}
	</div>
)));

export default TestListComponent;