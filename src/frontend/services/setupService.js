const setupService = (collectionName, before, after) => {
  return async function() {
    const client = this;
    const path = collectionName === 'users' ? collectionName : `api/${collectionName}`;
    if (client.service(path)) {
      client.service(path)
        .before(before)
        .after(after);
    }
  }
}

export default setupService;