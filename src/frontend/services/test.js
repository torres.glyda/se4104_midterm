import setupService from './setupService';

import Test from '../../models/Test';

import transform from '../../hooks/transform';

const testService = () => {
  const before = {};
  const after = {
    find: [
      transform(Test),
    ],
  };
  return setupService('tests', before, after);
}

export default testService;