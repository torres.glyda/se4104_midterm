import Realtime from 'feathers-offline-realtime';
import optimisticMutator from 'feathers-offline-realtime/lib/optimistic-mutator';

const setupUsersChoicesRealtime = () => {
  return async function() {
    const client = this;
    const usersChoicesRemote = client.service('api/usersChoices');
    const usersChoicesRealtime = new Realtime(usersChoicesRemote, { subscriber, uuid:true });

    client.use('client/usersChoices', optimisticMutator({ replicator: usersChoicesRealtime }));

    await usersChoicesRealtime.connect();
  }
}

export default setupUsersChoicesRealtime;