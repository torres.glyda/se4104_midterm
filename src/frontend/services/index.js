import test from './test';
import usersChoice from './usersChoice';
import usersChoiceRealtime from './realtime/userChoices';

const clientServices = () => {
  return function() {
    const client = this;
    client
      .configure(usersChoiceRealtime())
      .configure(usersChoice())
      .configure(test());
  }
}

export default clientServices;