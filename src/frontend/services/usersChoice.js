import setupService from './setupService';

const usersChoiceService = () => {
  const before = {};
  const after = {};
  return setupService('usersChoices', before, after);
}

export default usersChoiceService;