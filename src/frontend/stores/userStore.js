import { observable, action } from 'mobx';
import client from '../client';

class UserStore {
  @observable currentUser;

  @action.bound async setUser() {
    try {
      const user = await client.get('user');
      this.updateUser(user);
    } catch (error) {
      console.log(error);
    }
  }

  @action updateUser(newUser) {
    this.currentUser = newUser;
  }

  @action omitUser() {
    this.currentUser = undefined;
  }
}

export default new UserStore();