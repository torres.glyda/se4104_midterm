import { observable, action } from 'mobx';
import client from '../client';

import authStore from './authStore';
import userStore from './userStore';
import testStore from './testStore';

class AppStore {

  async setupApp() {
    await authStore.authenticate();
    testStore.retrieveData('tests', 'api/tests');
    testStore.retrieveData('usersChoices', 'client/usersChoices');
  }

}

export default new AppStore();