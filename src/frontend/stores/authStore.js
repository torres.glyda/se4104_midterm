import { observable, action } from 'mobx';
import client from '../client';

import userStore from './userStore';

class AuthenticationStore {
  @observable values = {
    username: '',
    password: '',
  };

  @action.bound setValue(event) {
    this.values[event.target.id] = event.target.value;
  }

  @action async authenticate(credentials) {
    try {
      const result = await client.authenticate(credentials);
      const payload = await client.passport.verifyJWT(result.accessToken);
      const user = await client.service('users').get(payload.userId)
      await client.set('user', user);
      userStore.setUser();
    } catch (error) {
      console.log(error);
    }
  }

  @action.bound async handleLogin() {
    this.authenticate({
      type: 'local',
      strategy: 'local',
      username: this.values.username,
      password: this.values.password
    });
  }

  @action.bound async handleLogout() {
    try {
      await client.logout();
      client.set('user', undefined);
      userStore.omitUser();
    } catch (error) {
      console.log('LOGOUT_FAILED');
      console.log(error);
    }
    this.resetValues();
  }

  @action resetValues() {
    Object.keys(this.values).forEach(value => {
      this.values[value] = '';
    });
  }
}

export default new AuthenticationStore();