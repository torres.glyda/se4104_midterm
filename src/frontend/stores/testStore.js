import { observable, action, computed } from 'mobx';
import client from '../client';

import userStore from './userStore';

class TestStore {
  @observable tests = [];
  @observable selectedTest = [];
  @observable usersChoices = [];
  @observable selectedTestUserChoices = {};

  constructor(){
    this.registerListeners();
  }

  async retrieveData(name, serviceName) {
    try {
      const results = await client.service(serviceName).find();
      this.updateProperty(name, results);
    } catch (error) {
      console.log(error);
    }
  }

  @action.bound updateProperty(prop, value) {
    this[prop] = value;
  }

  @action async selectTest(test) {
    this.updateProperty('selectedTest', test);
    const selectedTestUserChoices = this.usersChoices.find(usersChoice => usersChoice.testId === this.selectedTest._id);
    if (this.usersChoices.length === 0 || !selectedTestUserChoices) {
      this.selectedTestUserChoices.userChoicesIds = [];
      await client.service('client/usersChoices').create({
        userId: userStore.currentUser._id,
        testId: test._id,
        userChoicesIds: [],
      });
    } else {
      this.updateProperty('selectedTestUserChoices', selectedTestUserChoices);
      this.selectedTest.userChoices = this.userChoices;
    }
  }

  @computed get userChoices() {
    return this.selectedTest.choices.filter(choice =>
      this.isChoiceSelected(choice.choice._id)
    );
  }

  @action.bound async selectChoice(questionId, choice) {
    this.selectedTest.addChoice(questionId, choice);
    try {
      const id = this.selectedTestUserChoices.uuid;
      const userChoicesIds = this.selectedTest.userChoices.map(userChoice => userChoice.choice._id);
      await client.service('client/usersChoices').patch(id, { userChoicesIds });
    } catch (error) {
      console.log(error);
    }
  }

  get scoreDetails() {
    return `${this.selectedTest.totalScore}/${this.selectedTest.totalItems}`
  }

  @action.bound async resetTest() {
    try {
      const id = this.selectedTestUserChoices.uuid;
      await client.service('client/usersChoices').patch(id, { userChoicesIds: [] });
    } catch (error) {
      console.log(error);
    }
  }

  @action.bound isChoiceSelected(choiceId) {
    return this.selectedTestUserChoices.userChoicesIds.includes(choiceId);
  }

  registerListeners() {
    client.service('client/usersChoices')
      .on('patched', updatedUsersChoice => {
        this.selectedTestUserChoices.userChoicesIds = updatedUsersChoice.userChoicesIds;
      })
      .on('created', createdUsersChoice => {
        this.usersChoices.push(createdUsersChoice);
        this.updateProperty('selectedTestUserChoices', createdUsersChoice);
        this.selectedTest.userChoices = this.userChoices;
      });
  }

  removeListeners() {
    client.service('/local/messages')
      .off('created')
      .off('patched');
  }
}

export default new TestStore();