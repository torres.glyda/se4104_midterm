const theme = {
  primary: 'royalblue',
  secondary: 'tomato',
  accent1: 'dodgerblue',
  accent2: 'coral',
  accent3: 'white',
  accent4: 'gainsboro',
};

export default theme;