import styled, { injectGlobal } from 'styled-components';

const center = 'margin: 0 auto';

injectGlobal `
	body {
		font: 14px Arial, sans-serif;
		text-align: center;
	}

	.link {
		color: white;
		text-decoration: none;
	}
`;

export const AppContainer = styled.div `
	${center};
	width: 800px;
	height: 100%;
`;

export const Container = styled.div `
	${center};
	width: 600px;
	padding-bottom: 20px;
	border-radius: 5px;
	box-shadow: 0px 1px 5px rgba(0,0,0,0.3);
	p{
		font-size: 20px;
		font-weight: bold;
		-webkit-text-stroke: 0.5px black;
		padding: 10px 0 10px;
		background: ${({selected, theme}) =>  theme.primary};
		color: ${({selected, theme}) =>   theme.accent3};
	};
`

export const TestListItem = styled.div `
	margin-bottom: 3px;
	height: 40px;
	line-height: 40px;
	text-align: center;
	background: ${({theme}) =>  theme.accent2};
	color: ${({theme}) =>  theme.accent3};
	cursor: pointer;
	&:hover {
		background-color: ${({theme}) => theme.accent1};
	};
`;

export const Question = styled.h3 `
	fontSize: 25px;
	margin: 10px auto 10px;
`;

export const Button = styled.button `
	font-size: 15px;
	margin: 12px;
	padding: 4px 15px;
	cursor: pointer;
	color: ${({primary, theme}) => primary ? theme.accent3 : theme.primary};
	border: 2px solid ${({theme}) => theme.primary};
	border-radius: 3px;
	background-color: ${({primary, theme}) => primary ?  theme.primary : theme.accent3};
	&:hover {
		background-color: ${({primary, theme}) => primary ?  theme.secondary : theme.accent3};
		color: ${({primary, theme}) => primary ? theme.accent3 : theme.secondary};
		border-color: ${({primary, theme}) => theme.secondary};
	};
`;

export const Choice = styled.span `
	input {
		display: none;
	};
	label {
		display: inline-block;
		width: 100px;
		background-color: ${({theme}) => theme.accent4};
		padding: 5px 10px;
		font-size: 18px;
		cursor: pointer;
		margin-left: 3px;
	};
	input {
		&:checked+label, &:hover+label {
			color: ${({theme}) => theme.accent3};
			background-color: ${({theme}) => theme.secondary};
		};
	};
`;

export const Input = styled.input `
	padding: 8px;
	margin: 8px;
	color: ${({theme}) => theme.accent3};
	background: ${({theme}) => theme.secondary};
	border: none;
	border-radius: 3px;
`;