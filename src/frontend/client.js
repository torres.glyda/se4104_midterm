import feathers from 'feathers/client';
import hooks from 'feathers-hooks';
import authClient from 'feathers-authentication-client';
import feathersSocketio from 'feathers-socketio';
import socketioClient from 'socket.io-client';

import services from './services';

const client = feathers();
const socketConnection = socketioClient(`http://localhost:3000`);

client
  .configure(feathersSocketio(socketConnection))
  .configure(authClient({ storage: localStorage }))
  .configure(hooks())
  .configure(services())

window.app = client;

export default client;