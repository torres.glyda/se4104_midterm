import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';
import { HashRouter as Router } from 'react-router-dom';

import { ThemeProvider } from 'styled-components';
import theme from './styles/theme';

import App from './components/App';

import appStore from './stores/appStore';
import testStore from './stores/testStore';
import authStore from './stores/authStore';
import userStore from './stores/userStore';

const stores = {
	appStore,
	testStore,
	authStore,
	userStore,
};

(async () => {
	await appStore.setupApp();
	ReactDOM.render(
		<Provider {...stores}>
			<ThemeProvider theme={theme}>
				<App />
			</ThemeProvider>
		</Provider>,
		document.getElementById('app')
	);
})();