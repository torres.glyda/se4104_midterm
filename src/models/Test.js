import Model from './Model';

class Test extends Model {

  constructor({ _id, userChoices = [], questions = [], }) {
    super({ _id, userChoices, questions, });
    this.userChoices = userChoices;
    this.questions = questions;
  }

  get totalItems() {
    return this.questions.length;
  }

  isCorrect(choice) {
    return this.questions.some(question => question.correctAnswerID === choice._id);
  }

  get totalScore() {
    return this.userChoices.reduce((sum, userChoice) => sum + (this.isCorrect(userChoice.choice) ? 1 : 0), 0);
  }

  addChoice(questionId, choice) {
    const newUserChoice = { questionId, choice };
    const choiceIndex = this.userChoices.findIndex(userChoice => userChoice.questionId === questionId);
    if (choiceIndex > -1) {
      this.userChoices[choiceIndex] = newUserChoice;
    } else {
      this.userChoices.push(newUserChoice)
    }
  }

  get choices() {
    return this.questions.map(question =>
      question.choices.map(choice => { return { questionId: question._id, choice } })
    ).reduce((choices, choice) => choices.concat(choice), []);
  }
}

export default Test;