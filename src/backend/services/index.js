import authentication from './authentication';
import user from './user';
import choice from './choice';
import question from './question';
import test from './test';
import usersChoices from './usersChoices';

const services = (db) => {
  return function() {
    const app = this;
    app
      .configure(authentication())
      .configure(user(db))
      .configure(choice(db))
      .configure(question(db))
      .configure(test(db))
      .configure(usersChoices(db));
  }
}

export default services;