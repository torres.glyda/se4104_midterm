import setupService from './setupService';

import populate from '../../hooks/populate';

const setupTestService = (db) => {
  const after = {
    find: [
      populate('/api/questions', '_id', 'questionsIds', 'questions'),
    ],
  };
  const before = {};
  return function() {
    const testService = setupService(this, db, 'tests');
    testService
      .before(before)
      .after(after);
  }
}

export default setupTestService;