import feathersMongo from 'feathers-mongodb';

const setupService = (app, db, collectionName) => {
  const path = collectionName === 'users' ? collectionName : `api/${collectionName}`;
  app.use(path, feathersMongo({ Model: db.collection(collectionName) }));
  const service = app.service(path);
  return service;
}

export default setupService;