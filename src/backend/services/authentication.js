import auth from 'feathers-authentication';
import local from 'feathers-authentication-local';
import jwt from 'feathers-authentication-jwt';

const authenticationService = () => {
  return function() {
    const app = this;
    const before = {
      create: [
        auth.hooks.authenticate(['jwt', 'local'])
      ],
      remove: [
        auth.hooks.authenticate('jwt')
      ]
    };
    app
      .configure(auth(app.get('auth')))
      .configure(local())
      .configure(jwt());
    app.service('/authentication')
      .before(before);
  };
}

export default authenticationService;