import setupService from './setupService';

import queryWithCurrentUser from '../../hooks/queryWithCurrentUser';

const setupUsersChoicesService = (db) => {
  const after = {};
  const before = {
    find: [
      queryWithCurrentUser,
    ],
  };
  return function() {
    const testService = setupService(this, db, 'usersChoices');
    testService
      .before(before)
      .after(after);
  }
}

export default setupUsersChoicesService;