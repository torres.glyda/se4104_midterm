import setupService from './setupService';

import populate from '../../hooks/populate';

const setupQuestionService = (db) => {
  const after = {
    find: [
      populate('/api/choices', '_id', 'choicesIds', 'choices'),
    ],
  };
  const before = {};
  return function() {
    const questionService = setupService(this, db, 'questions');
    questionService
      .before(before)
      .after(after);
  }
}

export default setupQuestionService;