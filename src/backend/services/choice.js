import setupService from './setupService';

const setupChoiceService = (db) => {
  const after = {};
  const before = {};
  return function() {
    const choiceService = setupService(this, db, 'choices');
    choiceService
      .before(before)
      .after(after);
  }
}

export default setupChoiceService;