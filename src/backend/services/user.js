import setupService from './setupService';
import { hooks } from 'feathers-authentication-local';

const setupUserService = (db) => {
  const after = {};
  const before = {
    create: [
      hooks.hashPassword({ passwordField: 'password' }),
    ],
    update: [
      hooks.hashPassword({ passwordField: 'password' }),
    ],
    patch: [
      hooks.hashPassword({ passwordField: 'password' }),
    ],
  };
  return function() {
    const userService = setupService(this, db, 'users');
    userService
      .before(before)
      .after(after);
  }
}

export default setupUserService;