import Model from '../models/Model';

const transformArray = (ModelClass, data, prop) => {
  data[prop] = data[prop].map(object => new ModelClass(object));
}

const transformObject = (ModelClass, data, prop) => {
  data[prop] = new ModelClass(data[prop]);
}

const transformNested = (ModelClass, data, nestedProp) => {
  data.forEach(nestedData => {
    if (nestedData[nestedProp] instanceof Array) {
      transformArray(ModelClass, nestedData, nestedProp);
    } else if (nestedData[nestedProp] instanceof Object) {
      transformObject(ModelClass, nestedData, nestedProp);
    }
  })
}

const transform = (ModelClass, nestedProp) => (hook) => {
  const prop = hook.type === 'after' ? 'result' : 'data';
  if (hook[prop] instanceof Array) {
    if (!!nestedProp) {
      transformNested(ModelClass, hook[prop], nestedProp);
    } else {
      transformArray(ModelClass, hook, prop);
    }
  } else if (hook[prop] instanceof Object) {
    if (!!nestedProp) {
      transformNested(ModelClass, hook[prop], nestedProp);
    } else {
      transformObject(ModelClass, hook, prop);
    }
  } else {
    return Promise.reject('Invalid data type.')
  }
  return Promise.resolve(hook);
}

export default transform;