const queryWithCurrentUser = (hook) => {
  if (hook.params.user) {
    hook.params.query = { userId: hook.params.user._id };
  }
  return Promise.resolve(hook);
}

export default queryWithCurrentUser;