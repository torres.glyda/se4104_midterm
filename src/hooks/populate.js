const setData = async(specifiedData, service, foreignProp, localProp, nameAs, findOne) => {
  const data = specifiedData[localProp];
  const queryItem = data instanceof Array ? { $in: data } : data;
	const foreignData = await service.find({
    query: {
      [`${foreignProp}`]: queryItem
    }
  });
  specifiedData[nameAs] = findOne ? foreignData[0] : foreignData;
}

const populate = (serviceName, foreignProp, localProp, nameAs, findOne = false) => async(hook) => {
  const prop = hook.type === 'after' ? 'result' : 'data';
  let currentData = hook[prop];
	const service = await hook.app.service(serviceName);
  if (currentData instanceof Array) {
    await Promise.all(currentData.map(async(localData) =>
      await setData(localData, service, foreignProp, localProp, nameAs, findOne)
    ));
  } else if (currentData instanceof Object) {
    await setData(currentData, service, foreignProp, localProp, nameAs, findOne);
  }
  return Promise.resolve(hook);
}

export default populate;