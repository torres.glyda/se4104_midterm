import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';

import Test from '../../src/models/Test';
import transform from '../../src/hooks/transform';

chai.should();
chai.use(chaiAsPromised);

describe('transform hook', () => {
  let hook;
  let tests;
  beforeEach(() => {
    tests = [{
      question: 'Is this a question?'
    }];
  })

  it('transforms an object to Test Class', () => {
    hook = {
      type: 'after',
      result: tests[0],
      path: '/api/tests',
    };
    const transformHook = () => transform(Test)(hook);
    return transformHook().should.eventually.have.property('result').that.is.an.instanceof(Test);
  });

  it(`transforms an array's nested objects to Test Class`, () => {
    hook = {
      type: 'after',
      result: tests,
      path: '/api/tests',
    };
    const transformHook = () => transform(Test)(hook);
    const expectedHookResult = [new Test({
      question: 'Is this a question?'
    })];
    return transformHook().should.eventually.have.property('result').that.has.deep.members(expectedHookResult);
  });
});