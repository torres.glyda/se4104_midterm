import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';

import populate from '../../src/hooks/populate';

chai.should();
chai.use(sinonChai);
chai.use(chaiAsPromised);

describe('populate hook', () => {
  const findStub = sinon.stub();
  let hook;
  before(() => {
    const choices = [{
      _id: 123,
      choice: 'a'
    }, {
      _id: 452,
      choice: 'b'
    }, {
      _id: 555,
      choice: 'c'
    }];
    hook = {
      result: {
        choicesIds: [123, 555],
      },
      type: 'after',
      app: {
        service: (path) => {
          switch (path) {
            case '/api/choices':
              return { find: findStub }
          }
        }
      }
    };
    const find = (data, key) => {
      const results = [];
      data.forEach(user => hook.result[key].forEach(id => {
        if (user._id === id) {
          results.push(user)
        }
      }))
      return results;
    };
    findStub.returns(find(choices, 'choicesIds'))
  })

  it('populates choices ids', async() => {
    const populateHook = () => populate('/api/choices', '_id', 'choicesIds', 'choices')(hook);
    const hookResult = await populateHook();
    const expectedResult = [{ _id: 123, choice: 'a' }, { _id: 555, choice: 'c' }];
    hookResult.result.choices.should.have.deep.members(expectedResult);
  })
})