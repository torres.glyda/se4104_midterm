import * as chai from 'chai';
import Test from '../../src/models/Test';

chai.should();

describe('Test', () => {
  let test;
  beforeEach(() => {
    const testData = {
      questions: [{
        _id: 123,
        choices: [{
          _id: 1,
          choice: 'a'
        }, {
          _id: 2,
          choice: 'b'
        }, {
          _id: 3,
          choice: 'c'
        }],
        correctAnswerID: 1,
      }, {
        _id: 456,
        choices: [{
          _id: 4,
          choice: 'd'
        }, {
          _id: 5,
          choice: 'e'
        }, {
          _id: 6,
          choice: 'f'
        }],
        correctAnswerID: 1,
      }],
      userChoices: [{
        questionId: 123,
        choice: {
          _id: 2,
          choice: 'b'
        }
      }]
    }
    test = new Test(testData);
  })

  describe('#totalItems', () => {
    it('returns total items', () => {
      test.totalItems.should.equal(2);
    })
  })

  describe('#isCorrect()', () => {
    it('returns false if choice is incorrect', () => {
      test.isCorrect({
        _id: 2,
        choice: 'b'
      }).should.be.false;
    })

    it('returns true if choice is correct', () => {
      test.isCorrect({
        _id: 1,
        choice: 'a'
      }).should.be.true;
    })
  })

  describe('#totalScore', () => {
    it('returns total score', () => {
      test.totalScore.should.be.equal(0);
    })

    it('returns total score', () => {
      test.userChoices = [{
        questionId: 123,
        choice: {
          _id: 1,
          choice: 'a'
        }
      }];
      test.totalScore.should.be.equal(1);
    })
  })

  describe('#addChoice', () => {
    it('adds a new user choice to user choices array', () => {
      test.addChoice(456, { _id: 5, choice: 'e' });
      test.userChoices.should.have.lengthOf(2).and.deep.include(
        { questionId: 456, choice: { _id: 5, choice: 'e' } }
      );
    })

    it('replaces existing user choice from a question', () => {
      test.addChoice(123, { _id: 3, choice: 'c' });
      test.userChoices.should.have.deep.members([
        { questionId: 123, choice: { _id: 3, choice: 'c' } }
      ]);
    })
  })
})