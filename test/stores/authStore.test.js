import '../utils/dom';
import chai from 'chai';

import authStore from '../../src/frontend/stores/authStore';

chai.should();

describe('authStore', async() => {

  describe('#setValue()', () => {
    it('sets user credential values for username', () => {
      const event = { target: { id: 'username', value: 'useruser' } };
      authStore.setValue(event);
      authStore.values.username.should.equal('useruser');
    });

    it('sets user credential values for password', () => {
      const event = { target: { id: 'password', value: 'passweird' } };
      authStore.setValue(event);
      authStore.values.password.should.equal('passweird');
    });
  })

  describe('#resetValues()', () => {
    it('reset values to original state', () => {
      authStore.resetValues();
      authStore.values.should.deep.equal({
        username: '',
        password: '',
      });
    })
  })

})