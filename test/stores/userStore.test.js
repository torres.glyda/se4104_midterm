import '../utils/dom';
import * as client from '../../src/frontend/client';
import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';

import userStore from '../../src/frontend/stores/userStore';

chai.should();
chai.use(sinonChai);

describe('userStore', () => {
  let clientStub;
  before(() => {
    clientStub = sinon.stub(client);
    const user = {
      username: 'user',
    };
    clientStub.default.set('user', user);
  });

  describe('#setUser()', () => {
    it('sets current user', async() => {
      await userStore.setUser();
      userStore.currentUser.should.have.property('username').that.equals('user');
    })
  })

  describe('#updateUser()', () => {
    it('updates current user', () => {
      const newUser = {
        username: 'usertoo',
      };
      userStore.updateUser(newUser);
      userStore.currentUser.should.have.property('username').that.equals('usertoo');
    })
  })

  describe('#omitUser()', () => {
    it('removes current user', () => {
      userStore.omitUser();
      (typeof userStore.currentUser).should.be.equal('undefined');
    })
	})
	
})