const localStorageMock = () => {
  let localStorage = {};
  return {
    setItem: (prop, value) => {
      localStorage[prop] = value;
    },
    getItem: (prop) => {
      return localStorage[prop];
    },
    removeItem: (prop) => {
      Reflect.deleteProperty(localStorage, prop);
    },
    get length() {
      return Object.keys(localStorage).length;
    },
    key: (i) => {
      var keys = Object.keys(localStorage);
      return keys[i];
    },
    clear: () => {
      localStorage = {};
    }
  };
}

export default localStorageMock;