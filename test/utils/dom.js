import { JSDOM } from 'jsdom';
import localStorageMock from './localStorageMock';

const dom = new JSDOM('<!doctype html><html><body></body></html>', { url: "http://localhost:3000" });
const { window } = dom;

window.localStorage = localStorageMock();
global.localStorage = window.localStorage;
global.location = window.location;
global.window = window;